// Open bật tắt menu màn mobile 991px trở xuống
function OpenMenuMb() {
    $('.wp-menu-mb-js').addClass('open-menu-mb');
    $('.bg-menu-js').addClass('active');
    $('html,body').addClass('overflow-body');
}
function RemoveMenuMb() {
    $('.wp-menu-mb-js').removeClass('open-menu-mb');
    $('.bg-menu-js').removeClass('active');
    $('html,body').removeClass('overflow-body');
}
// JS click bật you



$(document).ready(function () {
    $("#playButton").click(function() {
        // Phát video và thêm class "active" cho "box-video__icon"
        $("#videoPlayer").attr("src", function(i, val) {
            return val + "&autoplay=1";
        });
        $('.box-video').addClass("open-video");
    });
    // Mở các select header
    $(".select-part-header .appear-els").click(function () {
      $(".select-part-header .appear-els").removeClass("active");
      $(".select-part-header .popup-select-list").removeClass("active");
      $(this).addClass("active");
      var $popup = $(this)
        .closest(".select-part-header")
        .find(".popup-select-list");
      $popup.addClass("active");
    });
  
    $(document).click(function (event) {
      var sttSelectHeader =
        $(event.target).parents(".select-part-header").length > 0;
      if (!sttSelectHeader) {
        $(".select-part-header .appear-els").removeClass("active");
        $(".select-part-header .popup-select-list").removeClass("active");
      }
    });
    // Scroll header
    $(window).scroll(function(event) {
        offsetAdd = $(window).scrollTop();
        if (offsetAdd > 0) {
            $('.menu-hoz-main-Kito').addClass('scroll');
        } else {
            $('.menu-hoz-main-Kito').removeClass('scroll');
        }
    });
   
});
  